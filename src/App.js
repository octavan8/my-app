import React, {Component} from 'react';
import './App.css';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import Table from 'react-bootstrap/Table';
// import { useState } from 'react';
// import { Button, Container, FormControl } from 'react-bootstrap';
class App extends Component{
  constructor(){
    super();
    this.state = { players: [] }
  }
  addPlayer = (e) => {
    e.preventDefault();

    let username = this.refs.username.value;
    let email = this.refs.email.value;
    let password = this.refs.password.value;

    this.state.players.push({username,email,password});
    this.setState({ players: this.state.players });

    this.refs.formulir.reset();
    this.refs.username.focus();
  }
  removePlayer = (i) => {
    this.state.players.splice(i,1);
    this.setState({players: this.state.players});
  }
  render(){
    return(
      <div>
        <h3>Tambah/Hapus Player</h3>
        <form ref="formulir">
          <input type="text" ref="username" placeholder="Username" />
          <input type="email" ref="email" placeholder="Email" />
          <input type="password" ref="password" placeholder="Password" />
          <button onClick={this.addPlayer}>Simpan</button>
        </form>
        <hr />
        <div>
          <ul>
            {this.state.players.map((data, i) => 
            <li key = {i}>
              <div>
                  <button onClick={()=>this.removePlayer(i)}>
                    hapus
                  </button> {data.username} | {data.email} | {data.password}
              </div>

            </li>
            )}
            
          </ul>
        </div>
      </div>
    )
  }
}


export default App;
